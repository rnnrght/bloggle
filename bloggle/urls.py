from django.conf.urls import patterns, include, url
from blag import views
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'bloggle.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', views.index, name='index'),
#    url(r'^blag/', include('bloggle.urls')),
    url(r'^blag/$', 'blag.views.index'),
    url(r'^blag/(?P<article_id>\d+)/$', 'blag.views.art'),
    url(r'^blag/submitarticle\.html', 'blag.views.artsub'),
    url(r'^submitarticle\.html', 'blag.views.artsub'),
    url(r'^admin/', include(admin.site.urls)),
)


