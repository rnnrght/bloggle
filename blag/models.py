from django.db import models
from django import forms
import datetime
# Create your models here.

class Article(models.Model):
    author = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    body  = models.TextField()
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title


class articleForm(forms.Form):
    title = forms.CharField(label="Title", max_length=200)
    pub_date = forms.DateTimeField(initial=datetime.datetime.now)
    author = forms.CharField(label="Author", max_length=200)
    body = forms.CharField(label="Body", max_length=2000,widget=forms.Textarea)
