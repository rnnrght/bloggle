from django.template import Context, loader
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from blag.models import Article, articleForm

# Create your views here.
def index(request):
    articleList = Article.objects.all().order_by('-pub_date')
    t = loader.get_template('blag/index.html')
    c = Context({
        'articleList': articleList,
    })
    return HttpResponse(t.render(c))

def art(request, article_id):
    article = Article.objects.get(id=article_id)
    t = loader.get_template('blag/article.html')
    c = Context({
        'article' : article,
        })
    return HttpResponse(t.render(c))

def artsub(request):
    if request.method == 'POST': # If the form has been submitted...
        form = articleForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            a = Article(author = form.cleaned_data['author'], 
            title = form.cleaned_data['title'],
            body  = form.cleaned_data['body'],
            pub_date = form.cleaned_data['pub_date'])
            a.save()
            return HttpResponseRedirect('/') # Redirect after POST
    else:
        form = articleForm() # An unbound form

    return render(request, 'blag/submitarticle.html', {
        'form': form,
    })
